package com.halas;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@MyAnnot(studentHome = "Railway station", course = 2)
public class Main {
    private static String name = "alex";

    public static void main(String[] args) throws IllegalAccessException, InvocationTargetException {

        //from Main get field %name% and get all annotations from Main and rename value in %name%
        Class<Main> clazz = Main.class;

        if (clazz.isAnnotationPresent(MyAnnot.class)) {
            MyAnnot annotation = clazz.getAnnotation(MyAnnot.class);
            Field[] mas = clazz.getDeclaredFields();

            for (Field o : mas) {
                System.out.println("Field \"" + o.getName() + "\" before: " + o.get(o.getName()));
                if (o.get(o.getName()) instanceof String) {
                    o.trySetAccessible();
                    String v = (String) o.get(o.getName());
                    o.set(o.getName(), "YURA");
                    System.out.println("Field \"" + o.getName() + "\" afrer: " + o.get(o.getName()));
                }
            }

            Annotation[] annotations = MyAnnot.class.getAnnotations();


            for (Annotation ant : annotations) {
                Field[] pol = ant.annotationType().getDeclaredFields();
                for (Field p : pol) {
                    System.out.println(p.getName());
                }
            }
            System.out.println("\n\nAnnotations fields::");
            System.out.println(annotation.studentName());
            System.out.println(annotation.studentHome());
            System.out.println(annotation.studentAge());
            System.out.println(annotation.course());
        } else {
            System.out.println("there is zero annotations!...badddddly");
        }

        //from %My% class get all field and get all methods
        Class my = My.class;
        My myObj=new My();

        Annotation[] ann = my.getAnnotations();
        System.out.println("\n\nAll annotations from class:    " + my.getName());
        for (Annotation a : ann) {
            System.out.println(a.toString());
        }
        System.out.println("\n\nInformation about fields: ");
        Field[] myFields = my.getDeclaredFields();
        for(Field f:myFields){
            f.setAccessible(true);
            System.out.println("Field: "+f.getName()+", type: "+f.getType().toString());
        }
        System.out.println("\n\nInformation about methods:");
        Method[] myMethod= my.getDeclaredMethods();
        for(Method m:myMethod){
            System.out.println("Method: "+m.getName());
            if(m.getName().equals("withOneParametr")){
                m.invoke(myObj,"Hello");
            }
        }
    }
}
