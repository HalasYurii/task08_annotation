package com.halas;

import java.util.Arrays;

@MyAnnot(studentHome = "Doroshenka", course = 3 )
public class My{
    private static String name;
    public int age;
    protected boolean flag;


    public void withOneParamert(String a){
        System.out.println("This is method: withOneParametrs("+a+")");
    }
    public void withTwoParametr(int c,String...a){
        System.out.println("This is method: withTwoParametrs("+c+")("+ Arrays.toString(a)+")");
    }
    @MyAnnot(studentHome = "someAnother", course = 2)
    private void someAnother(){
        System.out.println("This is method: someAnother()");
    }
}